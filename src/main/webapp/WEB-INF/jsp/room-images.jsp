<%@ page contentType="text/html;charset=UTF-8" isELIgnored ="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty sessionScope.language ? sessionScope.language : 'en'}" scope="session" />
<%--<fmt:setLocale value="${sessionScope.language}" scope="session" />--%>
<fmt:setBundle basename="content" scope="session"/>
<html>
<head>
    <title><fmt:message key="images"/> - Hotel Booking</title>
    <link rel="stylesheet" href="/css/images.css">
</head>
<body>
    <c:forEach items="${requestScope.images}" var="i">
        <img src="${i}" alt="room-image">
    </c:forEach>
</body>
</html>
