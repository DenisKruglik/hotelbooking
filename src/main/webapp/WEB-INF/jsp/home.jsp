<%@ page contentType="text/html;charset=UTF-8" isELIgnored ="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty sessionScope.language ? sessionScope.language : 'en'}" scope="session" />
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="content" scope="session"/>
<jsp:useBean id="now" class="java.util.Date"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="home"/> - Hotel Booking</title>
    <jsp:include page="/WEB-INF/html-parts/head-includes.html"/>
    <link rel="stylesheet" href="/css/home.css"/>
    <script src="/js/home.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/html-parts/header.jsp"/>
<main>
    <div class="card">
        <h2><fmt:message key="your-orders"/></h2>
        <input type="button" id="open-create-order-form" value="<fmt:message key="create-order"/>" class="button">
        <div class="order-wrapper"><c:forEach var="order" items="${requestScope.orders}">
                <div class="order card">
                    <div class="order-info">
                        <p><span class="property"><fmt:message key="start-date"/>:</span> <fmt:formatDate value="${order.getStartDate()}"/></p>
                        <p><span class="property"><fmt:message key="end-date"/>:</span> <fmt:formatDate value="${order.getEndDate()}"/></p>
                        <p><span class="property"><fmt:message key="room-type"/>:</span> ${order.getRoomType().getValue().toUpperCase()}</p>
                        <p><span class="property"><fmt:message key="places"/>:</span> ${order.getPlaces().getValue().toUpperCase()}</p>
                        <p class="<c:choose><c:when test="${order.getStatus().getValue() == 'approved'}">approved</c:when><c:when test="${order.getStatus().getValue() == 'rejected'}">rejected</c:when></c:choose>"><span class="property"><fmt:message key="status"/>:</span> ${order.getStatus().getValue().toUpperCase().replaceAll("_", " ")}</p>
                        <p>
                            <span class="property"><fmt:message key="message"/>:</span>
                            <p>
                                <c:choose>
                                    <c:when test="${not empty order.getMessage()}">
                                        ${fn:escapeXml(order.getMessage())}
                                    </c:when>
                                    <c:otherwise>-</c:otherwise>
                                </c:choose>
                            </p>
                        </p>
                        <p>
                            <span class="property"><fmt:message key="reply"/>:</span>
                            <p>
                                <c:choose>
                                    <c:when test="${not empty order.getReply()}">
                                        ${fn:escapeXml(order.getReply())}
                                    </c:when>
                                    <c:otherwise>-</c:otherwise>
                                </c:choose>
                            </p>
                        </p>
                        <p><span class="property"><fmt:message key="ordered-at"/>:</span> <fmt:formatDate value="${order.getOrderedAt()}"/></p>
                    </div>
                    <c:if test="${order.getStatus().getValue() != 'rejected'}">
                        <form method="post" action="/requester/?command=cancel_order">
                            <input type="hidden" name="id" value="${order.getId()}">
                            <input type="submit" class="button" value="<fmt:message key="cancel"/>">
                        </form>
                    </c:if>
                </div>
            </c:forEach></div>
        <ctg:pagination/>
    </div>
</main>
<jsp:include page="/WEB-INF/html-parts/footer.html"/>
<jsp:include page="/WEB-INF/html-parts/notification.html"/>
<div class="overlay">
    <div id="create-order-form" class="card">
        <h2><fmt:message key="create-order1"/></h2>
        <form action="/requester/?command=create_order" method="post">
            <label><span><fmt:message key="start-date"/></span><input type="date" name="start_date" required min="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>"></label>
            <label><span><fmt:message key="end-date"/></span><input type="date" name="end_date" required min="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>"></label>
            <select name="room_type" required>
                <option selected disabled value=""><fmt:message key="select-room-type"/></option>
                <option value="econom"><fmt:message key="econom"/></option>
                <option value="standart"><fmt:message key="standart"/></option>
                <option value="luxe"><fmt:message key="luxe"/></option>
            </select>
            <select name="places" required>
                <option selected disabled value=""></option>
                <option value="single"><fmt:message key="single"/></option>
                <option value="double"><fmt:message key="double"/></option>
                <option value="twin"><fmt:message key="twin"/></option>
                <option value="triple"><fmt:message key="triple"/></option>
            </select>
            <textarea name="message" placeholder="<fmt:message key="message-placeholder"/>"></textarea>
            <input type="submit" class="button" value="<fmt:message key="create-order2"/>" id="create-order">
        </form>
    </div>
</div>
</body>
</html>