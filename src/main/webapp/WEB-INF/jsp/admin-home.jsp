<%@ page contentType="text/html;charset=UTF-8" isELIgnored ="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty sessionScope.language ? sessionScope.language : 'en'}" scope="session" />
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="content" scope="session"/>
<jsp:useBean id="now" class="java.util.Date"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="home"/> - Hotel Booking</title>
    <jsp:include page="/WEB-INF/html-parts/head-includes.html"/>
    <link rel="stylesheet" href="/css/admin-home.css"/>
    <link rel="stylesheet" href="/css/table.css"/>
    <script src="/js/admin-home.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/html-parts/header.jsp"/>
<main>
    <div class="card">
        <div class="order-wrapper"><c:if test="${not empty requestScope.orders}">
            <table>
                <thead>
                <tr>
                    <td class="property"><fmt:message key="from"/></td>
                    <td class="property"><fmt:message key="start-date"/></td>
                    <td class="property"><fmt:message key="end-date"/></td>
                    <td class="property"><fmt:message key="room-type"/></td>
                    <td class="property"><fmt:message key="places"/></td>
                    <td class="property"><fmt:message key="message"/></td>
                    <td class="property"><fmt:message key="ordered-at"/></td>
                    <td class="property"><fmt:message key="status"/></td>
                    <td class="property"><fmt:message key="room-number"/></td>
                    <td class="property"><fmt:message key="assign-room"/></td>
                    <td class="property"><fmt:message key="reject"/></td>
                    <td class="property"><fmt:message key="check-in"/></td>
                    <td class="property"><fmt:message key="check-out"/></td>
                </tr>
                </thead>
                <tbody><c:forEach items="${requestScope.orders}" var="order">
                    <tr class="order" data-id="${order.getOrder().getId()}">
                        <td>${fn:escapeXml(order.getUsername())}</td>
                        <td><fmt:formatDate value="${order.getOrder().getStartDate()}"/></td>
                        <td><fmt:formatDate value="${order.getOrder().getEndDate()}"/></td>
                        <td>${order.getOrder().getRoomType().getValue().toUpperCase()}</td>
                        <td>${order.getOrder().getPlaces().getValue().toUpperCase()}</td>
                        <td><c:choose>
                            <c:when test="${not empty order.getOrder().getMessage()}">
                                ${fn:escapeXml(order.getOrder().getMessage())}
                            </c:when>
                            <c:otherwise>-</c:otherwise>
                        </c:choose></td>
                        <td><fmt:formatDate value="${order.getOrder().getOrderedAt()}"/></td>
                        <td>${order.getOrder().getStatus().getValue().toUpperCase().replace('_', ' ')}</td>
                        <td><c:choose>
                            <c:when test="${not empty order.getRoom()}">
                                ${order.getRoom().getNumber()}
                            </c:when>
                            <c:otherwise>-</c:otherwise>
                        </c:choose></td>
                        <td><c:choose>
                            <c:when test="${order.getOrder().getStatus().getValue() == 'pending'}">
                                <a href="/requester/?command=get_suitable_rooms&order=${order.getOrder().getId()}"><input type="button" class="button" value="<fmt:message key="assign-room-upper"/>"></a>
                            </c:when>
                            <c:otherwise>-</c:otherwise>
                        </c:choose></td>
                        <td><c:choose>
                            <c:when test="${order.getOrder().getStatus().getValue() == 'pending' || order.getOrder().getStatus().getValue() == 'approved'}">
                                <input type="button" class="button reject-button" value="<fmt:message key="reject-upper"/>">
                            </c:when>
                            <c:otherwise>-</c:otherwise>
                        </c:choose></td>
                        <td><c:choose>
                            <c:when test="${order.getOrder().getStatus().getValue() == 'approved'}">
                                <form method="post" action="/requester/?command=check_in">
                                    <input type="hidden" name="id" value="${order.getOrder().getId()}">
                                    <input type="submit" class="button" value="<fmt:message key="check-in-upper"/>">
                                </form>
                            </c:when>
                            <c:otherwise>-</c:otherwise>
                        </c:choose></td>
                        <td><c:choose>
                            <c:when test="${order.getOrder().getStatus().getValue() == 'checked_in'}">
                                <form method="post" action="/requester/?command=check_out">
                                    <input type="hidden" name="id" value="${order.getOrder().getId()}">
                                    <input type="submit" class="button" value="<fmt:message key="check-out-upper"/>">
                                </form>
                            </c:when>
                            <c:otherwise>-</c:otherwise>
                        </c:choose></td>
                    </tr>
                </c:forEach></tbody>
            </table>
            <ctg:pagination/>
        </c:if></div>
    </div>
</main>
<jsp:include page="/WEB-INF/html-parts/footer.html"/>
<jsp:include page="/WEB-INF/html-parts/notification.html"/>
<div class="overlay">
    <div id="reject-order-form" class="card">
        <h2><fmt:message key="reject-order"/></h2>
        <form action="/requester/?command=reject_order" method="post">
            <input type="hidden" name="id" required>
            <textarea name="reply" placeholder="<fmt:message key="reply-placeholder"/>"></textarea>
            <input type="submit" class="button" value="<fmt:message key="reject-order-upper"/>" id="reject-order">
        </form>
    </div>
</div>
</body>
</html>