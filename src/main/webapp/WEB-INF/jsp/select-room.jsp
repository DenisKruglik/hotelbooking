<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored ="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="language" value="${not empty param.language ? param.language : (not empty sessionScope.language ? sessionScope.language : 'en')}" scope="session" />
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="content" scope="session"/>
<jsp:useBean id="now" class="java.util.Date"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="select-room"/> - Hotel Booking</title>
    <jsp:include page="/WEB-INF/html-parts/head-includes.html"/>
    <link rel="stylesheet" href="/css/select-room.css"/>
    <link rel="stylesheet" href="/css/table.css">
    <script src="/js/select-room.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/html-parts/header.jsp"/>
<main>
    <div class="card">
        <h2><fmt:message key="select-room"/></h2>
        <div class="room-wrapper"><c:if test="${not empty requestScope.rooms}">
            <table>
                <thead>
                <tr>
                    <td class="property"><fmt:message key="preview"/></td>
                    <td class="property"><fmt:message key="number"/></td>
                    <td class="property"><fmt:message key="room-type"/></td>
                    <td class="property"><fmt:message key="places"/></td>
                    <td class="property"><fmt:message key="price"/></td>
                    <td class="property"><fmt:message key="more-images"/></td>
                    <td class="property"><fmt:message key="assign"/></td>
                </tr>
                </thead>
                <tbody><c:forEach items="${requestScope.rooms}" var="room">
                    <tr class="room" data-id="${room.getId()}">
                        <td><img src="${room.getPreview()}"></td>
                        <td>${room.getNumber()}</td>
                        <td>${room.getType().getValue().toUpperCase()}</td>
                        <td>${room.getPlaces().getValue().toUpperCase()}</td>
                        <td>${room.getPrice()}$</td>
                        <td><a href="/requester/?command=get_room_images&id=${room.getId()}" target="_blank"><input type="button" class="button" value="<fmt:message key="more-images-upper"/>"></a></td>
                        <td><input type="button" class="button assign-button" value="<fmt:message key="assign-upper"/>"></td>
                    </tr>
                </c:forEach></tbody>
            </table>
            <ctg:pagination/>
        </c:if></div>
    </div>
</main>
<jsp:include page="/WEB-INF/html-parts/footer.html"/>
<jsp:include page="/WEB-INF/html-parts/notification.html"/>
<div class="overlay">
    <div id="assign-room-form" class="card">
        <h2><fmt:message key="assign-room"/></h2>
        <form action="/requester/?command=assign_room" method="post">
            <input type="hidden" name="id" required>
            <textarea name="reply" placeholder="<fmt:message key="reply-placeholder1"/>"></textarea>
            <input type="submit" class="button" value="<fmt:message key="assign-room-upper"/>" id="assign-room">
        </form>
    </div>
</div>
</body>
</html>