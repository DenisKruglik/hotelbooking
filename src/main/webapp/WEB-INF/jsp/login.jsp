<%@ page contentType="text/html;charset=UTF-8" isELIgnored ="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty sessionScope.language ? sessionScope.language : 'en'}" scope="session" />
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="content" scope="session"/>

<!DOCTYPE html>
<html lang="${sessionScope.language}">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="login" /> - Hotel Booking</title>
    <jsp:include page="/WEB-INF/html-parts/head-includes.html"/>
    <link rel="stylesheet" href="/css/login.css"/>
    <script src="/js/login.js"></script>
</head>
<body>
    <jsp:include page="/WEB-INF/html-parts/login-header.jsp"/>
    <main>
        <div class="card">
            <h2><fmt:message key="welcome" /></h2>
            <div class="content">
                <article>
                    <img src="/img/hotel.jpg">
                    <p><fmt:message key="intro-info" /></p>
                </article>
                <div class="advantages">
                    <h3><fmt:message key="why-to-choose" /></h3>
                    <ul>
                        <li><fmt:message key="reason1" /></li>
                        <li><fmt:message key="reason2" /></li>
                        <li><fmt:message key="reason3" /></li>
                        <li><fmt:message key="reason4" /></li>
                        <li><fmt:message key="reason5" /></li>
                        <li><fmt:message key="reason6" /></li>
                        <li><fmt:message key="reason7" /></li>
                        <li><fmt:message key="reason8" /></li>
                        <li><fmt:message key="reason9" /></li>
                        <li><fmt:message key="reason10" /></li>
                    </ul>
                </div>
            </div>
            <div class="big-button">
                <input type="button" id="big-open-registration" class="button" value="<fmt:message key="register-now" />">
            </div>
        </div>
    </main>
    <jsp:include page="/WEB-INF/html-parts/footer.html"/>
    <jsp:include page="/WEB-INF/html-parts/notification.html"/>
    <div class="overlay">
        <div id="login-form" class="auth-form card">
            <h2><fmt:message key="login" /></h2>
            <form method="post" action="/requester/?command=login">
                <input type="text" name="username" placeholder="<fmt:message key="enter-username" />" required>
                <input type="password" name="password" placeholder="<fmt:message key="enter-password" />" required>
                <input type="submit" value="<fmt:message key="log-in" />" class="button">
            </form>
        </div>
        <div id="registration-form" class="auth-form card">
            <h2><fmt:message key="registration" /></h2>
            <form method="post" action="/requester/?command=register">
                <input type="text" name="username" placeholder="<fmt:message key="enter-username" />" required>
                <input type="password" name="password" placeholder="<fmt:message key="enter-password" />" required minlength="8">
                <input type="password" name="repeat_password" placeholder="<fmt:message key="repeat-password" />" required minlength="8">
                <input type="submit" value="<fmt:message key="register" />" class="button">
            </form>
        </div>
    </div>
</body>
</html>