<%@ page contentType="text/html;charset=UTF-8" isELIgnored ="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--<fmt:setLocale value="${sessionScope.language}" />--%>
<%--<fmt:setBundle basename="content" />--%>

<header>
    <div class="logo"><h1>Hotel Booking</h1></div>
    <ul class="auth-buttons">
        <li><input type="button" value="<fmt:message key="login-upper" />" id="login-open"></li>
        <li><input type="button" value="<fmt:message key="register" />" id="registration-open"></li>
    </ul>
    <form class="lang">
        <select id="language" name="language" onchange="submit()">
            <option value="en" ${sessionScope.language == 'en' ? 'selected' : ''}>English</option>
            <option value="ru" ${sessionScope.language == 'ru' ? 'selected' : ''}>Русский</option>
            <option value="be" ${sessionScope.language == 'be' ? 'selected' : ''}>Беларуская</option>
        </select>
    </form>
</header>