<%@ page contentType="text/html;charset=UTF-8" isELIgnored ="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--<fmt:setLocale value="${sessionScope.locale}" scope="session" />--%>
<%--<fmt:setBundle basename="content" var="rb" />--%>

<header>
    <div class="logo"><h1>Hotel Booking</h1></div>
    <div class="profile-info">
        <p class="greeting"><fmt:message key="logged-as"/> <span class="username">${fn:escapeXml(sessionScope.user.getUsername())}</span></p>
        <a href="/requester/?command=logout" class="logout"><input type="button" value="<fmt:message key="logout"/>"></a>
    </div>
    <form class="lang">
        <c:forEach var="p" items="${param.entrySet()}">
            <c:if test="${p.getKey() != 'language'}">
                <input type="hidden" name="${p.getKey()}" value="${p.getValue()}">
            </c:if>
        </c:forEach>
        <select id="language" name="language" onchange="submit()">
            <option value="en" ${sessionScope.language == 'en' ? 'selected' : ''}>English</option>
            <option value="ru" ${sessionScope.language == 'ru' ? 'selected' : ''}>Русский</option>
            <option value="be" ${sessionScope.language == 'be' ? 'selected' : ''}>Беларуская</option>
        </select>
    </form>
</header>