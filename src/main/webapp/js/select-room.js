$(function () {
    $('.assign-button').click(function(e){
        $('#assign-room-form input[name=id]').val($(this).parents('.room').attr('data-id'));
        $('.overlay').css('display', 'flex');
    });

    $('.overlay').click(function (e) {
        $(this).fadeOut(200);
    });

    $('#assign-room-form').click(function (e) {
        e.stopPropagation();
    });
});