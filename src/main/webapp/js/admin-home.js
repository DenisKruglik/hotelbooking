$(function () {
    $('.reject-button').click(function(e){
        $('#reject-order-form input[name=id]').val($(this).parents('.order').attr('data-id'));
        $('#reject-order-form textarea').val('');
        $('.overlay').css('display', 'flex');
    });

    $('.overlay').click(function (e) {
        $(this).fadeOut(200);
    });

    $('#reject-order-form').click(function (e) {
        e.stopPropagation();
    });
});