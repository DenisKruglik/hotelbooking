$(function () {
    $('#open-create-order-form').click(function(){
        $('.overlay').css('display', 'flex');
    });

    $('#create-order-form').click(function (e) {
        e.stopPropagation();
    });

    $('.overlay').click(function(){
        $(this).fadeOut(200);
    });

    $('#create-order-form input[name=start_date]').change(function(){
        $('#create-order-form input[name=end_date]').attr('min', this.value);
    });

    $('#create-order').click(function (e) {
        if ($('input[name=room_type]').val() === ''){
            e.preventDefault();
            notifyError('Select room type');
        }else if ($('input[name=places]').val() === ''){
            e.preventDefault();
            notifyError('Select places');
        }
    });
});