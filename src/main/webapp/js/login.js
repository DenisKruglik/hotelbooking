function openLoginForm(){
    $('#login-form').show();
    $('.overlay').css('display', 'flex');
}

function openRegistrationForm(){
    $('#registration-form').show();
    $('.overlay').css('display', 'flex');
}

function closeAuthForms(){
    $('.overlay').fadeOut(200, function () {
        $('.auth-form').hide();
    });
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

$(function () {
    if (findGetParameter('login_fail') !== null){
        notifyError('Username or password is incorrect');
    }

    var registrationError = findGetParameter('registration_fail');
    if (registrationError !== null){
        registrationError = registrationError.replace(/\+/g, ' ');
        notifyError(registrationError);
    }

    $('#login-open').click(openLoginForm);
    $('#registration-open, #big-open-registration').click(openRegistrationForm);
    $('.auth-form').click(function (e) {
        e.stopPropagation();
    });
    $('.overlay').click(closeAuthForms);
    $('#registration-form input[type=submit]').click(function (e) {
        if ($('#registration-form input[name=password]').val() !== $('#registration-form input[name=repeat_password]').val()){
            e.preventDefault();
            notifyError('Password and repeat password doesn\'t match');
        }
    });
});