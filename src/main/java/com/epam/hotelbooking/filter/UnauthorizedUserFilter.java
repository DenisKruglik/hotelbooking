package com.epam.hotelbooking.filter;

import com.epam.hotelbooking.model.command.CommandType;
import com.epam.hotelbooking.model.command.PagesNames;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.EnumSet;

public class UnauthorizedUserFilter implements Filter {

    private EnumSet<CommandType> allowedCommands = EnumSet.range(CommandType.LOGIN, CommandType.REGISTER);

    private final static String COMMAND_PARAMETER = "command";
    private final static Logger LOGGER = LogManager.getLogger(UnauthorizedUserFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String command = request.getParameter(COMMAND_PARAMETER);
        CommandType commandType = CommandType.fromValue(command);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (!allowedCommands.contains(commandType)){
            HttpSession session = request.getSession(false);
            if (session == null || session.getAttribute("user") == null){
                response.sendRedirect("/");
                LOGGER.info("Unauthorized user filtered");
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
