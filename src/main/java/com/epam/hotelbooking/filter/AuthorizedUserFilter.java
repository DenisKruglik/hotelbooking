package com.epam.hotelbooking.filter;

import com.epam.hotelbooking.model.command.PagesNames;
import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.entity.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthorizedUserFilter implements Filter {

    private final static Logger LOGGER = LogManager.getLogger(AuthorizedUserFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");
        if (user != null && request.getRequestURI().equals("/")){
            UserRole role = user.getRole();
            switch (role){
                case CLIENT:
                    response.sendRedirect(PagesNames.HOME_PAGE);
                    break;
                case ADMIN:
                    response.sendRedirect(PagesNames.ADMIN_HOME_PAGE);
                    break;
            }
            LOGGER.info("Authorized user redirected to home page");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
