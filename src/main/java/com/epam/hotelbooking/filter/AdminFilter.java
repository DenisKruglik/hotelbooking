package com.epam.hotelbooking.filter;

import com.epam.hotelbooking.model.command.CommandType;
import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.entity.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.EnumSet;

public class AdminFilter implements Filter {

    private final static String COMMAND_PARAMETER = "command";
    private final static Logger LOGGER = LogManager.getLogger(UnauthorizedUserFilter.class);

    private EnumSet<CommandType> adminCommands = EnumSet.range(CommandType.REJECT_ORDER, CommandType.GET_ROOM_IMAGES);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String command = request.getParameter(COMMAND_PARAMETER);
        CommandType commandType = CommandType.fromValue(command);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (adminCommands.contains(commandType)){
            HttpSession session = request.getSession(false);
            User user = (User) session.getAttribute("user");
            UserRole role = user.getRole();
            if (role != UserRole.ADMIN){
                response.sendRedirect("/");
                LOGGER.info("Access denied for non-admin user");
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
