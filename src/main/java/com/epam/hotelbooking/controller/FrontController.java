package com.epam.hotelbooking.controller;

import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.command.Command;
import com.epam.hotelbooking.model.exception.UnknownCommandException;
import com.epam.hotelbooking.model.command.CommandFactory;
import com.epam.hotelbooking.model.command.CommandResult;
import com.epam.hotelbooking.model.command.PagesNames;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class FrontController extends HttpServlet {

    private final static String COMMAND_PARAMETER = "command";
    private final static Logger LOGGER = LogManager.getLogger(FrontController.class);

    private CommandFactory factory = new CommandFactory();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        String commandName = request.getParameter(COMMAND_PARAMETER);
        try {
            Command command = factory.getCommand(commandName);
            CommandResult result = command.execute(request, response);
            CommandResult.ResultAction action = result.getAction();
            String viewName = result.getView();

            switch (action){
                case FORWARD:
                    RequestDispatcher view = request.getRequestDispatcher(viewName);
                    view.forward(request, response);
                    LOGGER.info("Forward to " + viewName);
                    break;
                case REDIRECT:
                    response.sendRedirect(viewName);
                    LOGGER.info("Redirected to " + viewName);
                    break;
            }

        }catch (UnknownCommandException e){
            LOGGER.warn(e.getMessage(), e);
            response.sendRedirect(PagesNames.HOME_PAGE);
        }catch (ServiceException e){
            LOGGER.warn(e.getMessage(), e);
            response.sendError(500);
        }

    }
}
