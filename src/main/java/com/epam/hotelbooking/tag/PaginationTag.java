package com.epam.hotelbooking.tag;

import com.epam.hotelbooking.dao.OrderDAO;
import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.entity.UserRole;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Map;

public class PaginationTag extends TagSupport {

    private final static String PAGE_PARAMETER = "page";
    private final static String COUNT_ATTRIBUTE = "count";

    @Override
    public int doStartTag() throws JspException{
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String currentPageString = request.getParameter(PAGE_PARAMETER);
        Integer currentPage = currentPageString != null ? Integer.parseInt(currentPageString) : 0;
        Integer orderCount = (Integer) request.getAttribute(COUNT_ATTRIBUTE);
        JspWriter out = pageContext.getOut();
        try {
            out.write("<ul class=\"pagination\">");
            Map parameters = request.getParameterMap();
            StringBuilder query = new StringBuilder();
            parameters.forEach((Object key, Object value) -> {
                String keyString = key.toString();
                String valueString = ((String[])value)[0];
                if (!keyString.equals(PAGE_PARAMETER)){
                    query.append(keyString + "=" + valueString + "&");
                }
            });
            String queryString = query.toString();
            if (currentPage > 0){
                out.write("<li><a href=\"/requester/?" + queryString + "page=" + (currentPage - 1) + "\">< Back</a></li>");
            }

            HttpSession session = request.getSession(true);
            User user = (User) session.getAttribute("user");
            UserRole role = user.getRole();
            Integer entitiesPerPage = role == UserRole.CLIENT ? OrderDAO.ENTITIES_PER_PAGE_CLIENT : OrderDAO.ENTITIES_PER_PAGE_ADMIN;

            int pagesAmount = (int) Math.ceil((double) orderCount / (double) entitiesPerPage);

            for (int i = 0; i < pagesAmount; i++){
                if (i != currentPage){
                    out.write("<li><a href=\"/requester/?" + queryString + "page=" + i + "\">" + (i + 1) + "</a></li>");
                }else{
                    out.write("<li>" + (i + 1) + "</li>");
                }
            }

            if (currentPage < pagesAmount - 1){
                out.write("<li><a href=\"/requester/?" + queryString + "page=" + (currentPage + 1) + "\">Forward ></a></li>");
            }
            out.write("</ul>");
        } catch (IOException e) {
            throw new JspException("Unable to create tag", e);
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag(){
        return EVAL_PAGE;
    }
}
