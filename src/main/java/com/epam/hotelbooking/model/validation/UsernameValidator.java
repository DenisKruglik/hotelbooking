package com.epam.hotelbooking.model.validation;

public class UsernameValidator {
    public boolean validate(String username){
        return !username.isEmpty();
    }
}
