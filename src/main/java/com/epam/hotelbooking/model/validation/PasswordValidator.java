package com.epam.hotelbooking.model.validation;

public class PasswordValidator {

    private final static int MIN_PASSWORD_LENGTH = 8;

    public boolean validate(String password){
        return password.length() >= MIN_PASSWORD_LENGTH;
    }
}
