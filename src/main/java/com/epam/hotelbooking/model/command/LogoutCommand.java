package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutCommand implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession(true);
        session.invalidate();
        return new CommandResult("/", CommandResult.ResultAction.REDIRECT);
    }
}
