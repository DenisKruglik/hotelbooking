package com.epam.hotelbooking.model.command;

public class CommandResult {
    private String view;
    private ResultAction action;

    public CommandResult(String view, ResultAction action){
        this.view = view;
        this.action = action;
    }

    public String getView() {
        return view;
    }

    public ResultAction getAction() {
        return action;
    }

    public enum ResultAction{
        FORWARD,
        REDIRECT
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommandResult)) return false;

        CommandResult result = (CommandResult) o;

        if (!getView().equals(result.getView())) return false;
        return getAction() == result.getAction();
    }

    @Override
    public int hashCode() {
        int result = getView().hashCode();
        result = 31 * result + getAction().hashCode();
        return result;
    }
}
