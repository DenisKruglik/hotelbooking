package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CancelOrderCommand implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String idString = request.getParameter("id");
        Integer id = Integer.parseInt(idString);
        OrderService service = new OrderService();
        service.cancelOrder(id);
        return new CommandResult(PagesNames.HOME_PAGE, CommandResult.ResultAction.REDIRECT);
    }
}
