package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.entity.dto.OrderDTO;
import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetActualOrdersCommand implements Command {

    private final static String PAGE_PARAMETER = "page";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String pageString = request.getParameter(PAGE_PARAMETER);
        Integer page;
        if (pageString == null){
            page = 0;
        }else{
            page = Integer.parseInt(pageString);
        }
        OrderService service = new OrderService();
        List<OrderDTO> orders = service.getActualOrders(page);
        Integer orderCount = service.countActualOrders();
        request.setAttribute("orders", orders);
        request.setAttribute("count", orderCount);
        return new CommandResult(PagesNames.ADMIN_HOME_PAGE_VIEW, CommandResult.ResultAction.FORWARD);
    }
}
