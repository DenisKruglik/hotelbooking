package com.epam.hotelbooking.model.command;

public class PagesNames {
    public final static String LOGIN_PAGE = "/requester/?command=get_static&route=login";
    public final static String HOME_PAGE = "/requester/?command=get_users_orders";
    public final static String HOME_PAGE_VIEW = "/WEB-INF/jsp/home.jsp";
    public final static String ADMIN_HOME_PAGE = "/requester/?command=get_actual_orders";
    public final static String ADMIN_HOME_PAGE_VIEW = "/WEB-INF/jsp/admin-home.jsp";
    public final static String SELECT_ROOM_VIEW = "/WEB-INF/jsp/select-room.jsp";
    public final static String ROOM_IMAGES_VIEW = "/WEB-INF/jsp/room-images.jsp";
    public final static String ERROR_PAGE_VIEW = "/WEB-INF/jsp/error.jsp";
}
