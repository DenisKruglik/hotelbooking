package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.exception.UnknownCommandException;

public class CommandFactory {

    public Command getCommand(String name) throws UnknownCommandException {
        Command command = null;
        CommandType type = CommandType.fromValue(name);

        if (type == null){
            throw new UnknownCommandException("Command " + name + " now found!");
        }

        switch (type){
            case LOGIN:
                command = new LoginCommand();
                break;
            case GET_STATIC:
                command = new GetStaticCommand();
                break;
            case LOGOUT:
                command = new LogoutCommand();
                break;
            case REGISTER:
                command = new RegisterCommand();
                break;
            case CREATE_ORDER:
                command = new CreateOrderCommand();
                break;
            case GET_USERS_ORDERS:
                command = new GetUsersOrdersCommand();
                break;
            case CANCEL_ORDER:
                command = new CancelOrderCommand();
                break;
            case REJECT_ORDER:
                command = new RejectOrderCommand();
                break;
            case GET_ACTUAL_ORDERS:
                command = new GetActualOrdersCommand();
                break;
            case GET_SUITABLE_ROOMS:
                command = new GetSuitableRoomsCommand();
                break;
            case ASSIGN_ROOM:
                command = new AssignRoomCommand();
                break;
            case CHECK_IN:
                command = new CheckInCommand();
                break;
            case CHECK_OUT:
                command = new CheckOutCommand();
                break;
            case GET_ROOM_IMAGES:
                command = new GetRoomImagesCommand();
                break;
            default:
                throw new UnknownCommandException("Command " + name + " now found!");
        }
        return command;
    }
}
