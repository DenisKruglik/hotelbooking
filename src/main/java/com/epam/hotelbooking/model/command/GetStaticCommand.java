package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GetStaticCommand implements Command {

    private final static String ROUTE_PARAMETER = "route";
    private final static String BASE_URL = "/WEB-INF/jsp/";
    private final static Logger LOGGER = LogManager.getLogger(GetStaticCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String route = request.getParameter(ROUTE_PARAMETER);
        if (route == null){
            throw new ServiceException("No route declared");
        }
        String result = BASE_URL + route + ".jsp";
        HttpSession session = request.getSession(true);
        session.setAttribute("location", route);
        LOGGER.info("User redirected to " + result);
        return new CommandResult(result, CommandResult.ResultAction.FORWARD);
    }
}
