package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.entity.Order;
import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GetUsersOrdersCommand implements Command {

    private final static String PAGE_PARAMETER = "page";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");
        Integer userId;
        if (user != null){
            userId = user.getId();
        }else{
            return new CommandResult(PagesNames.LOGIN_PAGE, CommandResult.ResultAction.REDIRECT);
        }
        String pageString = request.getParameter(PAGE_PARAMETER);
        Integer page;
        if (pageString == null){
            page = 0;
        }else{
            page = Integer.parseInt(pageString);
        }
        OrderService service = new OrderService();
        List<Order> orders = service.getUsersOrders(userId, page);
        Integer orderCount = service.countOrdersByUserId(userId);
        request.setAttribute("orders", orders);
        request.setAttribute("count", orderCount);
        return new CommandResult(PagesNames.HOME_PAGE_VIEW, CommandResult.ResultAction.FORWARD);
    }
}
