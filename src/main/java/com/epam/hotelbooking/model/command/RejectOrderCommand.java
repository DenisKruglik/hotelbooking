package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RejectOrderCommand implements Command {

    private final static String REPLY_PARAMETER = "reply";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String idString = request.getParameter("id");
        Integer id = Integer.parseInt(idString);
        String reply = request.getParameter(REPLY_PARAMETER);
        if (reply != null && reply.isEmpty()){
            reply = null;
        }
        OrderService service = new OrderService();
        service.rejectOrder(id, reply);
        return new CommandResult(PagesNames.ADMIN_HOME_PAGE, CommandResult.ResultAction.REDIRECT);
    }
}
