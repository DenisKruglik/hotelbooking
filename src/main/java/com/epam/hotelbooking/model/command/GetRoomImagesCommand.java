package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.RoomService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetRoomImagesCommand implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String roomIdString = request.getParameter("id");
        Integer roomId = Integer.parseInt(roomIdString);
        RoomService service = new RoomService();
        List<String> images = service.getRoomImages(roomId);
        request.setAttribute("images", images);
        return new CommandResult(PagesNames.ROOM_IMAGES_VIEW, CommandResult.ResultAction.FORWARD);
    }
}
