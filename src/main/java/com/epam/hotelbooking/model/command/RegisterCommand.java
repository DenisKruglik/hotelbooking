package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.exception.RegistrationException;
import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class RegisterCommand implements Command {

    private final static String USERNAME_PARAMETER = "username";
    private final static String PASSWORD_PARAMETER = "password";
    private final static String REPEAT_PASSWORD_PARAMETER = "repeat_password";
    private final static Logger LOGGER = LogManager.getLogger(RegisterCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String usernameObtained = request.getParameter(USERNAME_PARAMETER);
        String passwordObtained = request.getParameter(PASSWORD_PARAMETER);
        String repeatPasswordObtained = request.getParameter(REPEAT_PASSWORD_PARAMETER);

        User user = null;
        HttpSession session = request.getSession(true);

        try{
            UserService service = new UserService();
            user = service.register(usernameObtained, passwordObtained, repeatPasswordObtained);
        }catch (RegistrationException e){
            LOGGER.info("User failed to register");
            String message = null;
            try {
                message = URLEncoder.encode(e.getMessage(), "UTF-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            return new CommandResult("/?registration_fail=" + message, CommandResult.ResultAction.REDIRECT);
        }

        session.setAttribute("user", user);
        LOGGER.info("User " + usernameObtained + " successfully logged in");
        return new CommandResult(PagesNames.HOME_PAGE, CommandResult.ResultAction.REDIRECT);
    }
}
