package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.entity.UserRole;
import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class LoginCommand implements Command {

    private final static String USERNAME_PARAMETER = "username";
    private final static String PASSWORD_PARAMETER = "password";
    private final static Logger LOGGER = LogManager.getLogger(LoginCommand.class);

    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String usernameObtained = request.getParameter(USERNAME_PARAMETER);
        String passwordObtained = request.getParameter(PASSWORD_PARAMETER);

        User user = null;
        UserService service = new UserService();
        user = service.login(usernameObtained, passwordObtained);

        CommandResult result = null;
        HttpSession session = request.getSession(true);

        if (user != null){
            session.setAttribute("user", user);
            LOGGER.info("User " + usernameObtained + " successfully logged in");
            UserRole role = user.getRole();
            result = new CommandResult(role == UserRole.CLIENT ? PagesNames.HOME_PAGE : PagesNames.ADMIN_HOME_PAGE, CommandResult.ResultAction.REDIRECT);
        }else{
            LOGGER.info("User failed to login");
            result = new CommandResult("/?login_fail=1", CommandResult.ResultAction.REDIRECT);
        }

        return result;
    }
}
