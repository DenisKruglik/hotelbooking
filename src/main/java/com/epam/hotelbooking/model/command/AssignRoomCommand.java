package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AssignRoomCommand implements Command {

    private final static String ORDER_ID_ATTRIBUTE = "orderId";
    private final static String REPLY_PARAMETER = "reply";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String roomIdString = request.getParameter("id");
        Integer roomId = Integer.parseInt(roomIdString);
        HttpSession session = request.getSession(true);
        Integer orderId = (Integer) session.getAttribute(ORDER_ID_ATTRIBUTE);
        String reply = request.getParameter(REPLY_PARAMETER);
        OrderService service = new OrderService();
        service.assignRoomToOrder(orderId, roomId, reply);
        return new CommandResult(PagesNames.ADMIN_HOME_PAGE, CommandResult.ResultAction.REDIRECT);
    }
}
