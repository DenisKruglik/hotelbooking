package com.epam.hotelbooking.model.command;

public enum CommandType {
    LOGIN("login"),
    REGISTER("register"),
    GET_STATIC("get_static"),
    LOGOUT("logout"),
    CREATE_ORDER("create_order"),
    GET_USERS_ORDERS("get_users_orders"),
    CANCEL_ORDER("cancel_order"),
    REJECT_ORDER("reject_order"),
    GET_ACTUAL_ORDERS("get_actual_orders"),
    GET_SUITABLE_ROOMS("get_suitable_rooms"),
    ASSIGN_ROOM("assign_room"),
    CHECK_IN("check_in"),
    CHECK_OUT("check_out"),
    GET_ROOM_IMAGES("get_room_images");

    private String value;

    CommandType(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static CommandType fromValue(String value){
        CommandType[] values = CommandType.values();
        CommandType result = null;
        for (CommandType type: values){
            if (type.value.equals(value)){
                result = type;
            }
        }
        return result;
    }
}
