package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.entity.Order;
import com.epam.hotelbooking.model.entity.dto.RoomDTO;
import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.OrderService;
import com.epam.hotelbooking.model.service.RoomService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GetSuitableRoomsCommand implements Command {

    private final static String ORDER_PARAMETER = "order";
    private final static String PAGE_PARAMETER = "page";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession(true);
        String orderIdString = request.getParameter(ORDER_PARAMETER);
        String pageString = request.getParameter(PAGE_PARAMETER);
        Integer page;
        if (pageString == null){
            page = 0;
        }else{
            page = Integer.parseInt(pageString);
        }
        Integer orderId = Integer.parseInt(orderIdString);
        session.setAttribute("orderId", orderId);
        OrderService orderService = new OrderService();
        Order order = orderService.getById(orderId);
        RoomService roomService = new RoomService();
        List<RoomDTO> rooms = roomService.getSuitableRooms(order, page);
        Integer roomCount = roomService.countSuitableRooms(order);
        request.setAttribute("rooms", rooms);
        request.setAttribute("count", roomCount);
        return new CommandResult(PagesNames.SELECT_ROOM_VIEW, CommandResult.ResultAction.FORWARD);
    }
}
