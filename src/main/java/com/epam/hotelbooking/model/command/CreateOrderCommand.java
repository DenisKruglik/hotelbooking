package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.entity.Places;
import com.epam.hotelbooking.model.entity.RoomType;
import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Date;

public class CreateOrderCommand implements Command {

    private final static String START_DATE = "start_date";
    private final static String END_DATE = "end_date";
    private final static String ROOM_TYPE = "room_type";
    private final static String PLACES = "places";
    private final static String MESSAGE = "message";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");
        Integer userId;
        if (user != null){
            userId = user.getId();
        }else{
            return new CommandResult(PagesNames.LOGIN_PAGE, CommandResult.ResultAction.REDIRECT);
        }
        String startDateString = request.getParameter(START_DATE);
        String endDateString = request.getParameter(END_DATE);
        String roomTypeName = request.getParameter(ROOM_TYPE);
        String placesName = request.getParameter(PLACES);
        String message = request.getParameter(MESSAGE);
        if (message != null && message.isEmpty()){
            message = null;
        }

        Date startDate = Date.valueOf(startDateString);
        Date endDate = Date.valueOf(endDateString);
        RoomType roomType = RoomType.fromValue(roomTypeName);
        Places places = Places.fromValue(placesName);

        OrderService service = new OrderService();
        service.createOrder(userId, startDate, endDate, roomType, places, message);
        return new CommandResult(PagesNames.HOME_PAGE, CommandResult.ResultAction.REDIRECT);
    }
}
