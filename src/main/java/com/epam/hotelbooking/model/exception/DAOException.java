package com.epam.hotelbooking.model.exception;

public class DAOException extends Exception {
    public DAOException(String message){
        super(message);
    }

    public DAOException(String message, Throwable e){
        super(message, e);
    }
}
