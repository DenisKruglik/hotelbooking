package com.epam.hotelbooking.model.exception;

public class UnknownCommandException extends Exception {
    public UnknownCommandException(String message){
        super(message);
    }

    public UnknownCommandException(String message, Throwable e){
        super(message, e);
    }
}
