package com.epam.hotelbooking.model.exception;

import javax.servlet.ServletException;

public class ServiceException extends Exception {
    public ServiceException(String message){
        super(message);
    }

    public ServiceException(String message, Throwable e){
        super(message, e);
    }
}
