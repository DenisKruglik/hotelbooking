package com.epam.hotelbooking.model.service;

import com.epam.hotelbooking.dao.ConnectionPool;
import com.epam.hotelbooking.dao.RoomDAO;
import com.epam.hotelbooking.model.entity.Order;
import com.epam.hotelbooking.model.entity.Places;
import com.epam.hotelbooking.model.entity.RoomType;
import com.epam.hotelbooking.model.entity.dto.RoomDTO;
import com.epam.hotelbooking.model.exception.DAOException;
import com.epam.hotelbooking.model.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

public class RoomService {
    private final static Logger LOGGER = LogManager.getLogger(OrderService.class);

    private ConnectionPool pool = ConnectionPool.getInstance();

    public List<RoomDTO> getSuitableRooms(Order order, Integer page) throws ServiceException {
        Date startDate = order.getStartDate();
        Date endDate = order.getEndDate();
        RoomType type = order.getRoomType();
        Places places = order.getPlaces();
        List<RoomDTO> rooms = null;
        Connection connection = null;
        try {
            connection = pool.getConnection();
            RoomDAO dao = new RoomDAO(connection);
            rooms = dao.getSuitableRooms(startDate, endDate, type, places, page);
        } catch (DAOException e) {
            throw new ServiceException("Unable to get suitable rooms", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }
        return rooms;
    }

    public int countSuitableRooms(Order order) throws ServiceException{
        Date startDate = order.getStartDate();
        Date endDate = order.getEndDate();
        RoomType type = order.getRoomType();
        Places places = order.getPlaces();

        Connection connection = null;
        Integer roomCount = null;

        try {
            connection = pool.getConnection();
            RoomDAO dao = new RoomDAO(connection);
            roomCount = dao.countSuitableRooms(startDate, endDate, type, places);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }

        return roomCount;
    }

    public List<String> getRoomImages(Integer id) throws ServiceException{
        Connection connection = null;
        List<String> result;

        try {
            connection = pool.getConnection();
            RoomDAO dao = new RoomDAO(connection);
            result = dao.getImages(id);
        }catch (DAOException e){
            throw new ServiceException(e.getMessage(), e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }

        return result;
    }
}
