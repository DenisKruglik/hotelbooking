package com.epam.hotelbooking.model.service;

import com.epam.hotelbooking.dao.ConnectionPool;
import com.epam.hotelbooking.dao.UserDAO;
import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.exception.DAOException;
import com.epam.hotelbooking.model.exception.RegistrationException;
import com.epam.hotelbooking.model.exception.ServiceException;
import com.epam.hotelbooking.model.validation.PasswordValidator;
import com.epam.hotelbooking.model.validation.UsernameValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;

public class UserService {

    private final static Logger LOGGER = LogManager.getLogger(UserService.class);

    private ConnectionPool pool = ConnectionPool.getInstance();

    public User login(String username, String password) throws ServiceException{
        Connection connection = null;
        try {
            connection = pool.getConnection();
            UserDAO dao = new UserDAO(connection);
            return dao.getByUsernameAndPassword(username, password);
        } catch (DAOException e) {
            throw new ServiceException("Unable to check user", e);
        }finally {
            if (pool != null && connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Failed to return connection");
            }
        }
    }

    public User register(String username, String password, String repeatedPassword) throws ServiceException, RegistrationException{
        Connection connection = null;

        UsernameValidator usernameValidator = new UsernameValidator();
        if (!usernameValidator.validate(username)){
            throw new RegistrationException("Username is not valid");
        }

        PasswordValidator passwordValidator = new PasswordValidator();
        if (!passwordValidator.validate(password)){
            throw new RegistrationException("Password is not valid");
        }

        if (!password.equals(repeatedPassword)){
            throw new RegistrationException("Repeated password doesn\'t match password");
        }


        try {
            connection = pool.getConnection();
            UserDAO dao = new UserDAO(connection);
            User user = dao.getByUsername(username);

            if (user != null){
                throw new RegistrationException("User with such username already exists");
            }

            User userToSave = new User(username, password);
            dao.insert(userToSave);
            user = dao.getByUsername(username);
            LOGGER.info("User " + username + " successfully registered");
            return user;
        } catch (DAOException e) {
            throw new ServiceException("Unable to register user", e);
        }finally {
            if (pool != null && connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Failed to return connection");
            }
        }
    }
}
