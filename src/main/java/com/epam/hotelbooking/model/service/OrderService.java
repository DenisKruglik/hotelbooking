package com.epam.hotelbooking.model.service;

import com.epam.hotelbooking.dao.ConnectionPool;
import com.epam.hotelbooking.dao.OrderDAO;
import com.epam.hotelbooking.model.entity.Order;
import com.epam.hotelbooking.model.entity.OrderStatus;
import com.epam.hotelbooking.model.entity.Places;
import com.epam.hotelbooking.model.entity.RoomType;
import com.epam.hotelbooking.model.entity.dto.OrderDTO;
import com.epam.hotelbooking.model.exception.DAOException;
import com.epam.hotelbooking.model.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

public class OrderService {

    private final static Logger LOGGER = LogManager.getLogger(OrderService.class);

    private ConnectionPool pool = ConnectionPool.getInstance();

    public List<Order> getUsersOrders(Integer userId, Integer page) throws ServiceException{
        Connection connection = null;
        List<Order> orders = null;

        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            orders = dao.getByUserId(userId, page);
        } catch (DAOException e) {
            throw new ServiceException("Unable to get user's orders", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }

        return orders;
    }

    public void createOrder(Integer userId, Date startDate, Date endDate, RoomType roomType, Places places, String message) throws ServiceException{
        Order order = new Order();
        order.setUserId(userId);
        order.setStartDate(startDate);
        order.setEndDate(endDate);
        order.setRoomType(roomType);
        order.setPlaces(places);
        order.setMessage(message);

        Connection connection = null;
        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            dao.insert(order);
        }catch (DAOException e){
            throw new ServiceException("Unable to create an order", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }
    }

    public void cancelOrder(Integer id) throws ServiceException{
        Order order = getById(id);
        order.setStatus(OrderStatus.CANCELLED);

        Connection connection = null;
        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            dao.update(order);
        }catch (DAOException e){
            throw new ServiceException("Unable to cancel an order", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }
    }

    public int countOrdersByUserId(Integer userId) throws ServiceException{
        Connection connection = null;
        Integer orderCount = null;

        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            orderCount = dao.countOrdersByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }

        return orderCount;
    }

    public void rejectOrder(Integer id, String reply) throws ServiceException{
        Order order = getById(id);
        order.setStatus(OrderStatus.REJECTED);
        order.setReply(reply);

        Connection connection = null;
        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            dao.update(order);
        }catch (DAOException e){
            throw new ServiceException("Unable to reject an order", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }
    }

    public void checkIn(Integer id) throws ServiceException{
        Order order = getById(id);
        order.setStatus(OrderStatus.CHECKED_IN);

        Connection connection = null;
        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            dao.update(order);
        }catch (DAOException e){
            throw new ServiceException("Unable to check in", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }
    }

    public void checkOut(Integer id) throws ServiceException{
        Order order = getById(id);
        order.setStatus(OrderStatus.CHECKED_OUT);

        Connection connection = null;
        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            dao.update(order);
        }catch (DAOException e){
            throw new ServiceException("Unable to check out", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }
    }

    public List<OrderDTO> getActualOrders(Integer page) throws ServiceException{
        Connection connection = null;
        List<OrderDTO> orders = null;

        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            orders = dao.getActualOrders(page);
        } catch (DAOException e) {
            throw new ServiceException("Unable to get pending orders", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }

        return orders;
    }

    public int countActualOrders() throws ServiceException{
        Connection connection = null;
        Integer orderCount = null;

        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            orderCount = dao.countActualOrders();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }

        return orderCount;
    }

    public Order getById(Integer id) throws ServiceException{
        Connection connection = null;
        Order order = null;

        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            order = dao.getById(id);
        } catch (DAOException e) {
            throw new ServiceException("Unable to get order by id", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }

        return order;
    }

    public void assignRoomToOrder(Integer orderId, Integer roomId, String reply) throws ServiceException{
        Order order = getById(orderId);
        order.setStatus(OrderStatus.APPROVED);
        order.setRoomId(roomId);
        order.setReply(reply);

        Connection connection = null;
        try {
            connection = pool.getConnection();
            OrderDAO dao = new OrderDAO(connection);
            dao.update(order);
        }catch (DAOException e){
            throw new ServiceException("Unable to approve an order", e);
        }finally {
            if (connection != null){
                pool.returnConnection(connection);
            }else{
                LOGGER.warn("Unable to return connection");
            }
        }
    }
}
