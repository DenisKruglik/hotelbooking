package com.epam.hotelbooking.model.entity;

public enum OrderStatus {
    PENDING("pending"),
    APPROVED("approved"),
    CHECKED_IN("checked_in"),
    CHECKED_OUT("checked_out"),
    REJECTED("rejected"),
    CANCELLED("cancelled");

    private String value;

    OrderStatus(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static OrderStatus fromValue(String value){
        OrderStatus[] statuses = OrderStatus.values();
        OrderStatus result = null;

        for (OrderStatus status: statuses){
            if (status.value.equals(value)){
                result = status;
            }
        }

        return result;
    }
}
