package com.epam.hotelbooking.model.entity.dto;

import com.epam.hotelbooking.model.entity.Order;

public class OrderDTO {
    private Order order;
    private RoomDTO room;
    private String username;

    public OrderDTO(){}

    public OrderDTO(Order order, RoomDTO room, String username) {
        this.order = order;
        this.room = room;
        this.username = username;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public RoomDTO getRoom() {
        return room;
    }

    public void setRoom(RoomDTO room) {
        this.room = room;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDTO)) return false;

        OrderDTO orderDTO = (OrderDTO) o;

        if (getOrder() != null ? !getOrder().equals(orderDTO.getOrder()) : orderDTO.getOrder() != null) return false;
        if (getRoom() != null ? !getRoom().equals(orderDTO.getRoom()) : orderDTO.getRoom() != null) return false;
        return getUsername() != null ? getUsername().equals(orderDTO.getUsername()) : orderDTO.getUsername() == null;
    }

    @Override
    public int hashCode() {
        int result = getOrder() != null ? getOrder().hashCode() : 0;
        result = 31 * result + (getRoom() != null ? getRoom().hashCode() : 0);
        result = 31 * result + (getUsername() != null ? getUsername().hashCode() : 0);
        return result;
    }
}
