package com.epam.hotelbooking.model.entity;

import java.sql.Date;
import java.sql.Timestamp;

public class Order extends AbstractEntity {

    private Integer userId;
    private Date startDate;
    private Date endDate;
    private RoomType roomType;
    private Places places;
    private Integer roomId;
    private OrderStatus status;
    private String message;
    private String reply;
    private Timestamp orderedAt;

    public Order(){}

    public Order(int id, Integer userId, Date startDate, Date endDate, RoomType roomType, Places places, OrderStatus status) {
        super(id);
        this.userId = userId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.roomType = roomType;
        this.places = places;
        this.status = status;
    }

    public Order(int id, Integer userId, Date startDate, Date endDate, RoomType roomType, Places places, Integer roomId, OrderStatus status, String message, String reply, Timestamp orderedAt) {
        super(id);
        this.userId = userId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.roomType = roomType;
        this.places = places;
        this.roomId = roomId;
        this.status = status;
        this.message = message;
        this.reply = reply;
        this.orderedAt = orderedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        if (!super.equals(o)) return false;

        Order order = (Order) o;

        if (!getUserId().equals(order.getUserId())) return false;
        if (!getStartDate().equals(order.getStartDate())) return false;
        if (!getEndDate().equals(order.getEndDate())) return false;
        if (getRoomType() != order.getRoomType()) return false;
        if (getPlaces() != order.getPlaces()) return false;
        if (getRoomId() != null ? !getRoomId().equals(order.getRoomId()) : order.getRoomId() != null) return false;
        if (getStatus() != order.getStatus()) return false;
        if (getMessage() != null ? !getMessage().equals(order.getMessage()) : order.getMessage() != null) return false;
        if (getReply() != null ? !getReply().equals(order.getReply()) : order.getReply() != null) return false;
        return getOrderedAt() != null ? getOrderedAt().equals(order.getOrderedAt()) : order.getOrderedAt() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getUserId().hashCode();
        result = 31 * result + getStartDate().hashCode();
        result = 31 * result + getEndDate().hashCode();
        result = 31 * result + getRoomType().hashCode();
        result = 31 * result + getPlaces().hashCode();
        result = 31 * result + (getRoomId() != null ? getRoomId().hashCode() : 0);
        result = 31 * result + getStatus().hashCode();
        result = 31 * result + (getMessage() != null ? getMessage().hashCode() : 0);
        result = 31 * result + (getReply() != null ? getReply().hashCode() : 0);
        result = 31 * result + (getOrderedAt() != null ? getOrderedAt().hashCode() : 0);
        return result;
    }

    public String getReply() {

        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public Timestamp getOrderedAt() {
        return orderedAt;
    }

    public void setOrderedAt(Timestamp orderedAt) {
        this.orderedAt = orderedAt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public Places getPlaces() {
        return places;
    }

    public void setPlaces(Places places) {
        this.places = places;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
