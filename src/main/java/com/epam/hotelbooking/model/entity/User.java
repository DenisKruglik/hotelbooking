package com.epam.hotelbooking.model.entity;

import java.sql.Timestamp;
import java.util.Iterator;

public class User extends AbstractEntity {

    private String username;
    private String password;
    private Timestamp creationTime;
    private UserRole role;

    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public User(Integer id, String username, String password, Timestamp creationTime, UserRole userRole) {
        super(id);
        this.username = username;
        this.password = password;
        this.creationTime = creationTime;
        this.role = userRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        User user = (User) o;

        if (!getUsername().equals(user.getUsername())) {
            return false;
        }
        if (!getPassword().equals(user.getPassword())) {
            return false;
        }
        if (getCreationTime() != null ? !getCreationTime().equals(user.getCreationTime()) : user.getCreationTime() != null){
            return false;
        }
        return getRole() == user.getRole();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getUsername().hashCode();
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + (getCreationTime() != null ? getCreationTime().hashCode() : 0);
        result = 31 * result + getRole().hashCode();
        return result;
    }
}
