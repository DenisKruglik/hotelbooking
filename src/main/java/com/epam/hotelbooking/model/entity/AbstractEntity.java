package com.epam.hotelbooking.model.entity;

import java.io.Serializable;

public abstract class AbstractEntity implements Serializable {

    private Integer id;

    public AbstractEntity(){}
    public AbstractEntity(Integer id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractEntity)) {
            return false;
        }

        AbstractEntity that = (AbstractEntity) o;

        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }
}
