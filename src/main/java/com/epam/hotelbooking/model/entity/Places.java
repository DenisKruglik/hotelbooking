package com.epam.hotelbooking.model.entity;

public enum Places {
    SINGLE("single"),
    DOUBLE("double"),
    TWIN("twin"),
    TRIPLE("triple");

    private String value;

    Places(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Places fromValue(String value){
        Places[] places = Places.values();
        Places result = null;

        for (Places place: places){
            if (place.value.equals(value)){
                result = place;
            }
        }

        return result;
    }
}
