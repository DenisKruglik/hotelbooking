package com.epam.hotelbooking.model.entity;

public enum RoomType {
    ECONOM("econom"),
    STANDART("standart"),
    LUXE("luxe");

    private String value;

    RoomType(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static RoomType fromValue(String value){
        RoomType[] classes = RoomType.values();
        RoomType result = null;

        for (RoomType roomType : classes){
            if (roomType.value.equals(value)){
                result = roomType;
            }
        }

        return result;
    }
}
