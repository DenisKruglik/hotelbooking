package com.epam.hotelbooking.model.entity;

public enum UserRole {
    CLIENT("client"),
    ADMIN("admin");

    private String value;

    UserRole(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }

    public static UserRole fromValue(String value){
        UserRole[] types = UserRole.values();
        UserRole result = null;

        for (UserRole type: types){
            String typeValue = type.getValue();
            if (typeValue.equals(value)){
                result = type;
            }
        }
        return result;
    }
}
