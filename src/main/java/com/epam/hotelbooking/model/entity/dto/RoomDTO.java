package com.epam.hotelbooking.model.entity.dto;

import com.epam.hotelbooking.model.entity.AbstractEntity;
import com.epam.hotelbooking.model.entity.Places;
import com.epam.hotelbooking.model.entity.RoomType;

import java.math.BigDecimal;

public class RoomDTO extends AbstractEntity {
    private Integer number;
    private RoomType type;
    private Places places;
    private BigDecimal price;
    private String preview;

    public RoomDTO(){}

    public RoomDTO(Integer id, Integer number, RoomType type, Places places, BigDecimal price, String preview){
        super(id);
        this.number = number;
        this.type = type;
        this.places = places;
        this.price = price;
        this.preview = preview;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomDTO)) return false;
        if (!super.equals(o)) return false;

        RoomDTO roomDTO = (RoomDTO) o;

        if (getNumber() != null ? !getNumber().equals(roomDTO.getNumber()) : roomDTO.getNumber() != null) return false;
        if (getType() != roomDTO.getType()) return false;
        if (getPlaces() != roomDTO.getPlaces()) return false;
        if (getPrice() != null ? !getPrice().equals(roomDTO.getPrice()) : roomDTO.getPrice() != null) return false;
        return getPreview() != null ? getPreview().equals(roomDTO.getPreview()) : roomDTO.getPreview() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getNumber() != null ? getNumber().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getPlaces() != null ? getPlaces().hashCode() : 0);
        result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
        result = 31 * result + (getPreview() != null ? getPreview().hashCode() : 0);
        return result;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public RoomType getType() {
        return type;
    }

    public void setType(RoomType type) {
        this.type = type;
    }

    public Places getPlaces() {
        return places;
    }

    public void setPlaces(Places places) {
        this.places = places;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }
}
