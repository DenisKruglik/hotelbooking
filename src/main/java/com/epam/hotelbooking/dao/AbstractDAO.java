package com.epam.hotelbooking.dao;

import com.epam.hotelbooking.model.entity.AbstractEntity;
import com.epam.hotelbooking.model.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDAO<T extends AbstractEntity> {

    private final String GET_ALL_QUERY = "SELECT * FROM `" + getTableName() + "`;";
    private final String GET_BY_ID_QUERY = "SELECT * FROM `" + getTableName() + "` WHERE id=?;";
    private final String DELETE_BY_ID_QUERY = "DELETE FROM `" + getTableName() + "` WHERE id=?;";
    private final String INSERT_QUERY = "INSERT INTO `" + getTableName() + "` (" + getColumnsInsertString() + ") VALUES (" + getQuestionMarks() + ");";
    private final String UPDATE_QUERY = "UPDATE `" + getTableName() + "` SET " + getColumnsUpdateString() + " WHERE id=?;";
    private final static Logger LOGGER = LogManager.getLogger(AbstractDAO.class);

    private Connection connection;

    public AbstractDAO(Connection connection){
        this.connection = connection;
    }

    protected abstract String getTableName();
    protected abstract List<String> getColumns();
    protected abstract T construct(ResultSet resultSet) throws DAOException;
    protected abstract void setInsertData(PreparedStatement statement, T object) throws DAOException;

    protected Connection getConnection() {
        return connection;
    }

    private String getColumnsInsertString(){
        List<String> columns = getColumns();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < columns.size(); i++){
            String column = columns.get(i);
            result.append(column);
            if (i < columns.size() - 1){
                result.append(", ");
            }
        }
        return result.toString();
    }

    private String getColumnsUpdateString(){
        List<String> columns = getColumns();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < columns.size(); i++){
            String column = columns.get(i);
            result.append(column);
            result.append("=?");
            if (i < columns.size() - 1){
                result.append(", ");
            }
        }
        return result.toString();
    }

    private String getQuestionMarks(){
        List<String> columns = getColumns();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < columns.size(); i++){
            result.append("?");
            if (i < columns.size() - 1){
                result.append(", ");
            }
        }
        return result.toString();
    }

    public List<T> getAll() throws DAOException{
        List<T> objectList = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_QUERY);

            while (resultSet.next()){
                objectList.add(construct(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException("Failed to execute select query", e);
        }finally {
            closeStatement(statement);
        }
        return objectList;
    }

    public T getById(int id) throws DAOException{
        PreparedStatement statement = null;
        T object = null;

        try {
            statement = connection.prepareStatement(GET_BY_ID_QUERY);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                object = construct(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed to execute select by id query", e);
        }finally {
            closeStatement(statement);
        }

        return object;
    }

    public void delete(int id) throws DAOException{
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(DELETE_BY_ID_QUERY);
            statement.setInt(1, id);
            statement.executeUpdate();
        }catch (SQLException e){
            throw new DAOException("Failed to execute delete query", e);
        }finally {
            closeStatement(statement);
        }
    }

    public void delete(T object) throws DAOException{
        int id = object.getId();
        delete(id);
    }

    public void insert(T object) throws DAOException{
        PreparedStatement statement = null;

        try{
            statement = connection.prepareStatement(INSERT_QUERY);
            setInsertData(statement, object);
            statement.executeUpdate();
        }catch (SQLException e){
            throw new DAOException("Failed to execute insert query", e);
        }finally {
            closeStatement(statement);
        }
    }

    public void update(T object) throws DAOException{
        PreparedStatement statement = null;

        try{
            statement = connection.prepareStatement(UPDATE_QUERY);
            setInsertData(statement, object);
            List<String> columns = getColumns();
            int columnsSize = columns.size();
            int id = object.getId();
            statement.setInt(columnsSize + 1, id);
            statement.executeUpdate();
        }catch (SQLException e){
            throw new DAOException("Failed to execute update query", e);
        }finally {
            closeStatement(statement);
        }
    }

    protected List<T> executeQuery(String query, Object... args) throws DAOException{
        PreparedStatement statement = null;
        List<T> objectList = new ArrayList<>();

        try{
            statement = connection.prepareStatement(query);
            int i = 1;
            for (Object arg: args){
                statement.setObject(i, arg);
                i++;
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                objectList.add(construct(resultSet));
            }
        }catch (SQLException e){
            throw new DAOException("Failed to execute query", e);
        }finally {
            closeStatement(statement);
        }
        return objectList;
    }

    protected void executeUpdate(String query, Object... args) throws DAOException{
        PreparedStatement statement = null;

        try{
            statement = connection.prepareStatement(query);
            int i = 1;
            for (Object arg: args){
                statement.setObject(i, arg);
                i++;
            }
            statement.executeUpdate();
        }catch (SQLException e){
            throw new DAOException("Failed to execute query", e);
        }finally {
            closeStatement(statement);
        }
    }

    protected void closeStatement(Statement statement){
        if (statement != null){
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.warn("Failed to close statement", e);
            }
        }
    }
}
