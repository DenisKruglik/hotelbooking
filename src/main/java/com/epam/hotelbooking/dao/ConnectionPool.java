package com.epam.hotelbooking.dao;

import com.epam.hotelbooking.model.exception.DAOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {

    private final static String RESOURCE_BUNDLE = "database";
    private final static String URL_PROPERTY = "db.url";
    private final static String USER_PROPERTY = "db.user";
    private final static String PASSWORD_PROPERTY = "db.password";
    private final static String POOL_SIZE_PROPERTY = "db.poolSize";

    private static AtomicBoolean isInited = new AtomicBoolean(false);
    private static ConnectionPool instance;

    private final Semaphore semaphore;
    private final Queue<Connection> connections = new LinkedList<Connection>();
    private Lock lock = new ReentrantLock();

    private ConnectionPool(Queue<Connection> source) {
        int poolSize = source.size();
        semaphore = new Semaphore(poolSize, true);
        connections.addAll(source);
    }

    public static ConnectionPool getInstance(){
        if (!isInited.get()){
            init();
        }
        return instance;
    }

    private static void init(){
        ResourceBundle resource = ResourceBundle.getBundle(RESOURCE_BUNDLE);
        String url = resource.getString(URL_PROPERTY);
        String user = resource.getString(USER_PROPERTY);
        String pass = resource.getString(PASSWORD_PROPERTY);
        String poolSizeString = resource.getString(POOL_SIZE_PROPERTY);
        int poolSize = Integer.parseInt(poolSizeString);

        Queue<Connection> connections = new LinkedList<>();

        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());

            for (int i = 0; i < poolSize; i++){
                Connection connection = DriverManager.getConnection(url, user, pass);
                connections.add(connection);
            }
        }catch (SQLException e){
            throw new IllegalStateException("Unable to init connection pool", e);
        }

        instance = new ConnectionPool(connections);
        isInited.set(true);
    }


    public Connection getConnection() throws DAOException {
        try {
            semaphore.acquire();
            lock.lock();
            return connections.poll();
        } catch (InterruptedException e) {
            throw new DAOException("Failed to get connection", e);
        }finally {
            lock.unlock();
        }
    }

    public void returnConnection(Connection connection) {
        lock.lock();
        connections.add(connection);
        semaphore.release();
        lock.unlock();
    }

    @Override
    protected void finalize(){
        Iterator<Connection> iterator = connections.iterator();
        while (iterator.hasNext()){
            Connection connection = iterator.next();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
