package com.epam.hotelbooking.dao;

import com.epam.hotelbooking.model.entity.Order;
import com.epam.hotelbooking.model.entity.OrderStatus;
import com.epam.hotelbooking.model.entity.Places;
import com.epam.hotelbooking.model.entity.RoomType;
import com.epam.hotelbooking.model.entity.dto.OrderDTO;
import com.epam.hotelbooking.model.entity.dto.RoomDTO;
import com.epam.hotelbooking.model.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrderDAO extends AbstractDAO<Order> {

    public final static int ENTITIES_PER_PAGE_CLIENT = 3;
    public final static int ENTITIES_PER_PAGE_ADMIN = 10;
    private final static String TABLE_NAME = "orders";
    private final static String USER_ID_COLUMN = "user_id";
    private final static String START_DATE_COLUMN = "start_date";
    private final static String END_DATE_COLUMN = "end_date";
    private final static String ROOM_TYPE_COLUMN = "room_type";
    private final static String PLACES_COLUMN = "places";
    private final static String ROOM_ID_COLUMN = "room_id";
    private final static String STATUS_COLUMN = "status";
    private final static String MESSAGE_COLUMN = "message";
    private final static String REPLY_COLUMN = "reply";
    private final static String ORDERED_AT_COLUMN = "ordered_at";
    private final static String USERNAME_COLUMN = "username";
    private final static String ROOM_NUMBER_COLUMN = "number";
    private final static String GET_BY_USER_ID_QUERY = "SELECT * FROM `" + TABLE_NAME + "` WHERE user_id=? AND status IN ('pending', 'approved', 'rejected') ORDER BY id DESC LIMIT " + ENTITIES_PER_PAGE_CLIENT + " OFFSET ?;";
    private final static String INSERT_NEW_ORDER_QUERY = "INSERT INTO `" + TABLE_NAME + "` (user_id, start_date, end_date, room_type, places, message) VALUES (?, ?, ?, ?, ?, ?);";
    private final static String UPDATE_QUERY = "UPDATE `" + TABLE_NAME + "` SET status=?, reply=?, room_id=? WHERE id=?;";
    private final static String COUNT_ORDERS_BY_USER_ID_QUERY = "SELECT COUNT(id) FROM `" + TABLE_NAME + "` WHERE user_id=? AND status IN ('pending', 'approved', 'rejected')";
    private final static String GET_ACTUAL_ORDERS_QUERY = "SELECT o.*, u.username, r.number FROM `" + TABLE_NAME + "` o INNER JOIN users u ON o.user_id=u.id " +
            "LEFT JOIN rooms r ON o.room_id=r.id " +
            "WHERE status IN ('pending', 'approved', 'checked_in') ORDER BY id DESC LIMIT " + ENTITIES_PER_PAGE_ADMIN + " OFFSET ?;";
    private final static String COUNT_ACTUAL_ORDERS_QUERY = "SELECT COUNT(id) FROM `" + TABLE_NAME + "` WHERE status IN ('pending', 'approved', 'checked_in');";

    public OrderDAO(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected List<String> getColumns() {
        return Arrays.asList(
                USER_ID_COLUMN,
                START_DATE_COLUMN,
                END_DATE_COLUMN,
                ROOM_TYPE_COLUMN,
                PLACES_COLUMN,
                ROOM_ID_COLUMN,
                STATUS_COLUMN,
                MESSAGE_COLUMN,
                REPLY_COLUMN,
                ORDERED_AT_COLUMN
        );
    }

    @Override
    protected Order construct(ResultSet resultSet) throws DAOException {
        Order order;
        try {
            Integer id = resultSet.getInt("id");
            Integer userId = resultSet.getInt(USER_ID_COLUMN);
            Date startDate = resultSet.getDate(START_DATE_COLUMN);
            Date endDate = resultSet.getDate(END_DATE_COLUMN);
            String roomTypeName = resultSet.getString(ROOM_TYPE_COLUMN);
            RoomType roomType = RoomType.fromValue(roomTypeName);
            String placesName = resultSet.getString(PLACES_COLUMN);
            Places places = Places.fromValue(placesName);
            Integer roomId = resultSet.getInt(ROOM_ID_COLUMN);
            if (roomId == 0){
                roomId = null;
            }
            String statusName = resultSet.getString(STATUS_COLUMN);
            OrderStatus status = OrderStatus.fromValue(statusName);
            String message = resultSet.getString(MESSAGE_COLUMN);
            String reply = resultSet.getString(REPLY_COLUMN);
            Timestamp orderedAt = resultSet.getTimestamp(ORDERED_AT_COLUMN);
            order = new Order(id, userId, startDate, endDate, roomType, places, roomId, status, message, reply, orderedAt);
        } catch (SQLException e) {
            throw new DAOException("Unable to construct object from obtained data", e);
        }
        return order;
    }

    @Override
    protected void setInsertData(PreparedStatement statement, Order object) throws DAOException {
        Integer userId = object.getUserId();
        Date startDate = object.getStartDate();
        Date endDate = object.getEndDate();
        RoomType roomType = object.getRoomType();
        String roomTypeTitle = roomType.getValue();
        Places places = object.getPlaces();
        String placesTitle = places.getValue();
        Integer roomId = object.getRoomId();
        OrderStatus status = object.getStatus();
        String statusTitle = status.getValue();
        String message = object.getMessage();
        String reply = object.getReply();
        Timestamp orderedAt = object.getOrderedAt();

        try{
            statement.setInt(1, userId);
            statement.setDate(2, startDate);
            statement.setDate(3, endDate);
            statement.setString(4, roomTypeTitle);
            statement.setString(5, placesTitle);
            statement.setInt(6, roomId);
            statement.setString(7, statusTitle);
            statement.setString(8, message);
            statement.setString(9, reply);
            statement.setTimestamp(10, orderedAt);
        }catch (SQLException e){
            throw new DAOException("Unable to set sql query parameters", e);
        }
    }

    public List<Order> getByUserId(Integer userId, Integer page) throws DAOException{
        int offset = page * ENTITIES_PER_PAGE_CLIENT;
        return executeQuery(GET_BY_USER_ID_QUERY, userId, offset);
    }

    @Override
    public void insert(Order order) throws DAOException{
        Integer userId = order.getUserId();
        Date startDate = order.getStartDate();
        Date endDate = order.getEndDate();
        RoomType roomType = order.getRoomType();
        String roomTypeName = roomType.getValue();
        Places places = order.getPlaces();
        String placesName = places.getValue();
        String message = order.getMessage();
        executeUpdate(INSERT_NEW_ORDER_QUERY, userId, startDate, endDate, roomTypeName, placesName, message);
    }

    @Override
    public void update(Order order) throws DAOException{
        Integer id = order.getId();
        OrderStatus status = order.getStatus();
        String statusName = status.getValue();
        String reply = order.getReply();
        Integer roomId = order.getRoomId();
        System.out.println(roomId);
        executeUpdate(UPDATE_QUERY, statusName, reply, roomId, id);
    }

    public int countOrdersByUserId(Integer userId) throws DAOException{
        Connection connection = getConnection();
        Integer result = null;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(COUNT_ORDERS_BY_USER_ID_QUERY);
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("Unable to count orders", e);
        }finally {
            closeStatement(statement);
        }
        return result;
    }

    public List<OrderDTO> getActualOrders(Integer page) throws DAOException{
        int offset = page * ENTITIES_PER_PAGE_ADMIN;
        Connection connection = getConnection();
        PreparedStatement statement = null;
        List<OrderDTO> orders = new ArrayList<>();

        try{
            statement = connection.prepareStatement(GET_ACTUAL_ORDERS_QUERY);
            statement.setInt(1, offset);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                OrderDTO orderDTO = new OrderDTO();
                Order order = construct(resultSet);
                orderDTO.setOrder(order);
                String username = resultSet.getString(USERNAME_COLUMN);
                orderDTO.setUsername(username);
                Integer roomNumber = resultSet.getInt(ROOM_NUMBER_COLUMN);
                if (roomNumber != 0){
                    RoomDTO room = new RoomDTO();
                    room.setNumber(roomNumber);
                    orderDTO.setRoom(room);
                }
                orders.add(orderDTO);
            }
        }catch (SQLException e){
            throw new DAOException("Failed to get actual orders", e);
        }finally {
            closeStatement(statement);
        }
        return orders;
    }

    public int countActualOrders() throws DAOException{
        Connection connection = getConnection();
        Integer result = null;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(COUNT_ACTUAL_ORDERS_QUERY);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("Unable to count orders", e);
        }finally {
            closeStatement(statement);
        }
        return result;
    }
}
