package com.epam.hotelbooking.dao;

import com.epam.hotelbooking.model.entity.Places;
import com.epam.hotelbooking.model.entity.RoomType;
import com.epam.hotelbooking.model.entity.dto.RoomDTO;
import com.epam.hotelbooking.model.exception.DAOException;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RoomDAO extends AbstractDAO<RoomDTO> {
    private final static int ENTITIES_PER_PAGE = 3;
    private final static String NUMBER_COLUMN = "number";
    private final static String TYPE_COLUMN = "type";
    private final static String PLACES_COLUMN = "places";
    private final static String PRICE_COLUMN = "price";
    private final static String PREVIEW_COLUMN = "preview";
    private final static String GET_ALL_ROOMS_QUERY = "SELECT r.id AS id, r.number AS number, t.type AS type, t.places AS places, p.price AS price, i.path AS preview FROM " +
            "rooms r LEFT JOIN images i ON r.preview_id=i.id " +
            "INNER JOIN room_types t ON r.room_type=t.id " +
            "INNER JOIN prices p ON p.type_id=t.id " +
            "WHERE p.end_date IS NULL";
    private final static String GET_ROOM_BY_ID_QUERY = "SELECT r.id AS id, r.number AS number, t.type AS type, t.places AS places, p.price AS price, i.path AS preview FROM " +
            "rooms r LEFT JOIN images i ON r.preview_id=i.id " +
            "INNER JOIN room_types t ON r.room_type=t.id " +
            "INNER JOIN prices p ON p.type_id=t.id " +
            "WHERE p.end_date IS NULL AND r.id=? LIMIT 1";
    private final static String GET_SUITABLE_ROOMS_QUERY = "SELECT r.id AS id, r.number AS number, t.type AS type, t.places AS places, p.price AS price, i.path AS preview FROM " +
            "rooms r LEFT JOIN images i ON r.preview_id=i.id " +
            "INNER JOIN room_types t ON r.room_type=t.id " +
            "INNER JOIN prices p ON p.type_id=t.id " +
            "WHERE p.end_date IS NULL AND " +
            "(SELECT COUNT(o.id) FROM orders o WHERE o.room_id=r.id AND status IN ('pending', 'approved', 'checked_in') AND (start_date between ? and ? or ? between start_date and end_date))=0 AND " +
            "t.type=? AND t.places=? " +
            "LIMIT " + ENTITIES_PER_PAGE + " OFFSET ?;";
    private final static String COUNT_SUITABLE_ROOMS_QUERY = "SELECT COUNT(r.id) FROM " +
            "rooms r INNER JOIN room_types t ON r.room_type=t.id " +
            "WHERE (SELECT COUNT(id) FROM orders WHERE room_id=r.id AND status IN ('pending', 'approved', 'checked_in') AND (start_date between ? and ? or ? between start_date and end_date))=0 AND " +
            "t.type=? AND t.places=?;";
    private final static String GET_IMAGES_QUERY = "SELECT i.path " +
            "FROM rooms r INNER JOIN images_has_rooms h ON r.id=h.room_id " +
            "INNER JOIN images i ON i.id=h.image_id " +
            "WHERE r.id=?;";

    public RoomDAO(Connection connection){
        super(connection);
    }

    @Override
    public List<RoomDTO> getAll() throws DAOException {
        return executeQuery(GET_ALL_ROOMS_QUERY);
    }

    @Override
    public RoomDTO getById(int id) throws DAOException {
        return executeQuery(GET_ROOM_BY_ID_QUERY, id).get(0);
    }

    public List<RoomDTO> getSuitableRooms(Date startDate, Date endDate, RoomType type, Places places, Integer page) throws DAOException {
        int offset = page * ENTITIES_PER_PAGE;
        String typeName = type.getValue();
        String placesName = places.getValue();
        return executeQuery(GET_SUITABLE_ROOMS_QUERY, startDate, endDate, startDate, typeName, placesName, offset);
    }

    public int countSuitableRooms(Date startDate, Date endDate, RoomType type, Places places) throws DAOException{
        String typeName = type.getValue();
        String placesName = places.getValue();
        Connection connection = getConnection();
        Integer result = null;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(COUNT_SUITABLE_ROOMS_QUERY);
            statement.setDate(1, startDate);
            statement.setDate(2, endDate);
            statement.setDate(3, startDate);
            statement.setString(4, typeName);
            statement.setString(5, placesName);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("Unable to count orders", e);
        }finally {
            closeStatement(statement);
        }
        return result;
    }

    public List<String> getImages(Integer id) throws DAOException{
        Connection connection = getConnection();
        List<String> images = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(GET_IMAGES_QUERY);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                String image = resultSet.getString(1);
                images.add(image);
            }
        }catch (SQLException e){
            throw new DAOException("Unable to get images", e);
        }finally {
            closeStatement(statement);
        }
        return images;
    }

    @Override
    protected String getTableName() {
        return null;
    }

    @Override
    protected List<String> getColumns() {
        return Collections.EMPTY_LIST;
    }

    @Override
    protected RoomDTO construct(ResultSet resultSet) throws DAOException {
        RoomDTO room;
        try {
            Integer id = resultSet.getInt("id");
            Integer number = resultSet.getInt(NUMBER_COLUMN);
            String roomTypeName = resultSet.getString(TYPE_COLUMN);
            RoomType roomType = RoomType.fromValue(roomTypeName);
            String placesName = resultSet.getString(PLACES_COLUMN);
            Places places = Places.fromValue(placesName);
            BigDecimal price = resultSet.getBigDecimal(PRICE_COLUMN);
            String preview = resultSet.getString(PREVIEW_COLUMN);
            room = new RoomDTO(id, number, roomType, places, price, preview);
        } catch (SQLException e) {
            throw new DAOException("Unable to construct object from obtained data", e);
        }
        return room;
    }

    @Override
    protected void setInsertData(PreparedStatement statement, RoomDTO object) throws DAOException {
        throw new UnsupportedOperationException("No insertions for rooms");
    }

    @Override
    public void delete(int id) throws DAOException {
        throw new UnsupportedOperationException("No deletions for rooms");
    }

    @Override
    public void delete(RoomDTO object) throws DAOException {
        throw new UnsupportedOperationException("No deletions for rooms");
    }

    @Override
    public void insert(RoomDTO object) throws DAOException {
        throw new UnsupportedOperationException("No insertions for rooms");
    }

    @Override
    public void update(RoomDTO object) throws DAOException {
        throw new UnsupportedOperationException("No updating for rooms");
    }
}
