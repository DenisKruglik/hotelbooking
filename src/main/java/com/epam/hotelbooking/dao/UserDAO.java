package com.epam.hotelbooking.dao;

import com.epam.hotelbooking.model.entity.User;
import com.epam.hotelbooking.model.entity.UserRole;
import com.epam.hotelbooking.model.exception.DAOException;

import java.sql.*;
import java.util.Arrays;
import java.util.List;

public class UserDAO extends AbstractDAO<User> {

    private final static String TABLE_NAME = "users";
    private final static String USERNAME_COLUMN = "username";
    private final static String PASSWORD_COLUMN = "password";
    private final static String CREATION_TIME_COLUMN = "creation_time";
    private final static String ROLE_COLUMN = "role";
    private final static String GET_BY_USERNAME_AND_PASSWORD_QUERY = "SELECT * FROM `" + TABLE_NAME + "` WHERE username=? AND password=MD5(?);";
    private final static String GET_BY_USERNAME_QUERY = "SELECT * FROM `" + TABLE_NAME + "` WHERE username=?;";
    private final static String INSERT_NEW_USER_QUERY = "INSERT INTO `" + TABLE_NAME + "` (username, password) VALUES (?, MD5(?));";

    public UserDAO(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected List<String> getColumns() {
        String[] columns = {USERNAME_COLUMN, PASSWORD_COLUMN, CREATION_TIME_COLUMN, ROLE_COLUMN};
        return Arrays.asList(columns);
    }

    @Override
    protected User construct(ResultSet resultSet) throws DAOException{
        try {
            Integer id = resultSet.getInt("id");
            String username = resultSet.getString(USERNAME_COLUMN);
            String password = resultSet.getString(PASSWORD_COLUMN);
            Timestamp createTime = resultSet.getTimestamp(CREATION_TIME_COLUMN);
            String roleTitle = resultSet.getString(ROLE_COLUMN);
            UserRole role = UserRole.fromValue(roleTitle);
            return new User(id, username, password, createTime, role);
        } catch (SQLException e) {
            throw new DAOException("Unable to construct object from obtained data", e);
        }
    }

    @Override
    protected void setInsertData(PreparedStatement statement, User object) throws DAOException {
        String username = object.getUsername();
        String password = object.getPassword();
        Timestamp createTime = object.getCreationTime();
        UserRole type = object.getRole();
        String typeTitle = type.getValue();

        try {
            statement.setString(1, username);
            statement.setString(2, password);
            statement.setTimestamp(3, createTime);
            statement.setString(4, typeTitle);
        } catch (SQLException e) {
            throw new DAOException("Unable to set sql query parameters", e);
        }
    }

    public User getByUsernameAndPassword(String username, String password) throws DAOException{
        List<User> userList = executeQuery(GET_BY_USERNAME_AND_PASSWORD_QUERY, username, password);
        if (userList.isEmpty()){
            return null;
        }
        return userList.get(0);
    }

    public User getByUsername(String username) throws DAOException{
        List<User> userList = executeQuery(GET_BY_USERNAME_QUERY, username);
        if (userList.isEmpty()){
            return null;
        }
        return userList.get(0);
    }

    @Override
    public void insert(User user) throws DAOException{
        String username = user.getUsername();
        String password = user.getPassword();

        executeUpdate(INSERT_NEW_USER_QUERY, username, password);
    }
}
