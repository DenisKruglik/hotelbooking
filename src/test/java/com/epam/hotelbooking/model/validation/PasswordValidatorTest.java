package com.epam.hotelbooking.model.validation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PasswordValidatorTest {
    private PasswordValidator validator = new PasswordValidator();

    @Test
    public void shouldReturnTrueWhenPasswordIsValid(){
//        given
        String password = "12345678";
//        when
        boolean result = validator.validate(password);
//        then
        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnFalseWhenPasswordIsInvalid(){
//        given
        String password = "12345";
//        when
        boolean result = validator.validate(password);
//        then
        Assert.assertFalse(result);
    }
}
