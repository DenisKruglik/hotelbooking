package com.epam.hotelbooking.model.validation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UsernameValidatorTest {
    private UsernameValidator validator = new UsernameValidator();

    @Test
    public void shouldReturnTrueWhenUsernameIsValid(){
//        given
        String username = "bananchik";
//        when
        boolean result = validator.validate(username);
//        then
        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnFalseWhenUsernameIsInvalid(){
//        given
        String username = "";
//        when
        boolean result = validator.validate(username);
//        then
        Assert.assertFalse(result);
    }
}
