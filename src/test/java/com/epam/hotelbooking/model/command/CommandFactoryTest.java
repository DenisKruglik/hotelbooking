package com.epam.hotelbooking.model.command;

import com.epam.hotelbooking.model.exception.UnknownCommandException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CommandFactoryTest {
    private CommandFactory factory = new CommandFactory();

    @Test(dataProvider = "commands")
    public void shouldReturnCommand(String commandTitle, Class clazz) throws UnknownCommandException {
        Command command = factory.getCommand(commandTitle);
        boolean isInstance = clazz.isInstance(command);
        Assert.assertTrue(isInstance);
    }

    @Test(expectedExceptions = UnknownCommandException.class, dataProvider = "invalidCommands")
    public void shouldThrowUnknownCommandException(String commandTitle) throws UnknownCommandException {
        Command command = factory.getCommand(commandTitle);
    }

    @DataProvider(name = "commands")
    public Object[][] commands(){
        return new Object[][]{
            {"login", LoginCommand.class},
            {"register", RegisterCommand.class},
            {"get_static", GetStaticCommand.class},
            {"logout", LogoutCommand.class},
            {"create_order", CreateOrderCommand.class},
            {"get_users_orders", GetUsersOrdersCommand.class},
            {"cancel_order", CancelOrderCommand.class},
            {"reject_order", RejectOrderCommand.class},
            {"get_actual_orders", GetActualOrdersCommand.class},
            {"get_suitable_rooms", GetSuitableRoomsCommand.class},
            {"assign_room", AssignRoomCommand.class},
            {"check_in", CheckInCommand.class},
            {"check_out", CheckOutCommand.class},
            {"get_room_images", GetRoomImagesCommand.class}
        };
    }

    @DataProvider(name = "invalidCommands")
    public Object[] invalidCommands(){
        return new Object[]{
            "sdjfndn",
            null
        };
    }
}
