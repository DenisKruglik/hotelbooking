-- MySQL dump 10.13  Distrib 5.7.15, for Win64 (x86_64)
--
-- Host: localhost    Database: hotel_booking
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'/img/r1.jpg'),(2,'/img/r2.jpg'),(3,'/img/r3.jpg'),(4,'/img/r4.jpg'),(5,'/img/r5.jpg'),(6,'/img/r6.jpg'),(7,'/img/r7.jpg'),(8,'/img/r8.jpg'),(9,'/img/r9.jpg');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images_has_rooms`
--

DROP TABLE IF EXISTS `images_has_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images_has_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_has_rooms_rooms1_idx` (`room_id`),
  KEY `fk_images_has_rooms_images1_idx` (`image_id`),
  CONSTRAINT `fk_images_has_rooms_images1` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_images_has_rooms_rooms1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images_has_rooms`
--

LOCK TABLES `images_has_rooms` WRITE;
/*!40000 ALTER TABLE `images_has_rooms` DISABLE KEYS */;
INSERT INTO `images_has_rooms` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,2),(5,5,2),(6,6,2),(7,7,3),(8,8,3),(9,9,3);
/*!40000 ALTER TABLE `images_has_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `room_type` enum('econom','standart','luxe') NOT NULL,
  `places` enum('single','double','twin','triple') NOT NULL,
  `room_id` int(10) unsigned DEFAULT NULL,
  `status` enum('pending','approved','checked_in','checked_out','rejected','cancelled') NOT NULL DEFAULT 'pending',
  `message` text,
  `reply` text,
  `ordered_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `applicant_id_idx` (`user_id`),
  KEY `assigned_room_id_idx` (`room_id`),
  CONSTRAINT `applicant_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assigned_room_id` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,'2018-04-11','2018-04-14','econom','double',NULL,'cancelled','I would like the room on the sunny side',NULL,'2018-04-05 20:03:59'),(2,1,'2018-04-04','2018-04-06','standart','single',NULL,'cancelled','',NULL,'2018-04-05 20:13:43'),(3,1,'2018-04-10','2018-04-11','luxe','double',NULL,'cancelled','',NULL,'2018-04-05 20:14:04'),(4,1,'2018-04-28','2018-05-04','econom','single',NULL,'cancelled','',NULL,'2018-04-05 20:14:27'),(5,1,'2018-04-06','2018-04-10','standart','twin',NULL,'cancelled','',NULL,'2018-04-05 20:15:14'),(6,1,'2018-04-04','2018-04-04','standart','double',NULL,'cancelled',NULL,NULL,'2018-04-05 20:23:14'),(7,1,'2018-04-13','2018-04-15','standart','single',NULL,'cancelled',NULL,NULL,'2018-04-05 20:23:28'),(8,1,'2018-04-12','2018-04-13','luxe','double',NULL,'rejected',NULL,'Sorry, but we didn\'t manage to find the room that would suite your preferences.','2018-04-05 20:23:44'),(9,1,'2018-04-04','2018-04-04','standart','single',NULL,'cancelled',NULL,NULL,'2018-04-05 20:23:55'),(10,1,'2018-04-13','2018-04-13','standart','single',NULL,'rejected',NULL,'No room for you','2018-04-06 23:16:36'),(11,1,'2018-04-23','2018-04-26','econom','single',NULL,'cancelled',NULL,NULL,'2018-04-23 21:34:28'),(12,1,'2018-04-23','2018-04-24','econom','double',NULL,'cancelled',NULL,NULL,'2018-04-24 19:32:43'),(13,1,'2018-04-23','2018-04-24','econom','single',NULL,'cancelled',NULL,NULL,'2018-04-24 19:41:40'),(14,1,'2018-04-23','2018-04-24','standart','single',NULL,'checked_out',NULL,NULL,'2018-04-24 19:47:05'),(15,1,'2018-04-23','2018-04-23','standart','single',NULL,'checked_out',NULL,NULL,'2018-04-24 19:48:14'),(16,1,'2018-04-26','2018-04-28','standart','single',1,'rejected',NULL,NULL,'2018-04-25 15:55:54'),(17,1,'2018-04-27','2018-04-28','standart','single',1,'rejected',NULL,NULL,'2018-04-25 16:12:58'),(18,1,'2018-04-26','2018-04-28','standart','single',1,'cancelled',NULL,'(^_^)','2018-04-25 16:14:19'),(19,1,'2018-04-28','2018-04-29','standart','single',1,'checked_out',NULL,'','2018-04-25 16:14:40'),(20,1,'2018-04-24','2018-04-25','standart','single',NULL,'cancelled',NULL,NULL,'2018-04-25 16:36:40'),(21,1,'2018-04-24','2018-04-26','standart','single',NULL,'checked_out',NULL,NULL,'2018-04-25 16:41:23'),(22,1,'2018-04-24','2018-04-25','standart','single',NULL,'rejected',NULL,NULL,'2018-04-25 16:42:34'),(23,1,'2018-04-26','2018-04-27','standart','single',NULL,'rejected',NULL,NULL,'2018-04-25 16:42:47'),(24,1,'2018-04-24','2018-04-26','standart','single',NULL,'rejected',NULL,NULL,'2018-04-25 16:48:30'),(25,1,'2018-04-27','2018-04-28','standart','single',1,'checked_out',NULL,'','2018-04-25 16:48:45'),(26,1,'2018-04-29','2018-04-29','standart','single',NULL,'cancelled',NULL,NULL,'2018-04-25 16:49:12'),(27,1,'2018-04-24','2018-04-25','standart','single',NULL,'rejected',NULL,'¯\\_(ツ)_/¯','2018-04-25 17:34:16'),(28,1,'2018-04-24','2018-04-25','standart','single',1,'checked_in',NULL,'Вось Ваш нумар','2018-04-25 17:52:18'),(29,1,'2018-04-26','2018-04-27','standart','single',1,'checked_out',NULL,'You\'re welcome','2018-04-25 17:52:31'),(30,1,'2018-05-03','2018-05-04','standart','single',NULL,'cancelled','Хочу комнату',NULL,'2018-05-04 15:02:40'),(31,1,'2018-05-03','2018-05-04','standart','single',1,'approved','Комнату мне','This is the most suitable room for u','2018-05-04 15:03:17');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prices`
--

DROP TABLE IF EXISTS `prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL,
  `price` decimal(10,0) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_with_price_idx` (`type_id`),
  CONSTRAINT `type_with_price` FOREIGN KEY (`type_id`) REFERENCES `room_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prices`
--

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;
INSERT INTO `prices` VALUES (1,1,100,'2018-04-06',NULL),(2,2,200,'2018-04-06',NULL);
/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_types`
--

DROP TABLE IF EXISTS `room_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `places` enum('single','double','twin','triple') NOT NULL,
  `type` enum('econom','standart','luxe') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_types`
--

LOCK TABLES `room_types` WRITE;
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
INSERT INTO `room_types` VALUES (1,'single','standart'),(2,'twin','luxe');
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL,
  `room_type` int(10) unsigned NOT NULL,
  `preview_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_preview_idx` (`preview_id`),
  KEY `room_type_idx` (`room_type`),
  CONSTRAINT `room_preview` FOREIGN KEY (`preview_id`) REFERENCES `images` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `room_type` FOREIGN KEY (`room_type`) REFERENCES `room_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,101,1,1),(2,102,2,4),(3,103,2,7);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `creation_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `role` enum('client','admin') NOT NULL DEFAULT 'client',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Dendrarij','25d55ad283aa400af464c76d713c07ad','2018-04-03 07:51:34','client'),(2,'admin','0192023a7bbd73250516f069df18b500','2018-04-05 21:07:29','admin'),(3,'Vova','8fd3df6d68f46965dc9d93b3e6480ffe','2018-04-07 11:51:52','client');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 17:08:38
